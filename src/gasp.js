console.clear();

const palets = [chroma.scale(['#7900FF', '#548CFF', '#93FFD8', '#CFFFDC']), chroma.scale(['#B983FF', '#94B3FD', '#94DAFF', '#99FEFF']), chroma.scale(['#3DB2FF', '#FFEDDA', '#FFB830', '#FF2442'])];

class Ball {
  constructor() {
    let minR = width * 0.025;
    let maxR = width * 0.05;
    this.x = random((maxR / 2 + 5), width - (maxR / 2 + 5));
    this.y = random((maxR / 2 + 5), height - (maxR / 2 + 5));
    this.r = random(minR, maxR);
    this.fill = Math.random();
    this.v = {
      x: random(1, 5) * random([-1, 1]),
      y: random(1, 5) * random([-1, 1])
    };
  }
  update() {
    if (this.x + this.v.x > width - this.r || this.x + this.v.x < this.r) {
      this.v.x = -this.v.x;
    }
    if (this.y + this.v.y > height - this.r || this.y + this.v.y < this.r) {
      this.v.y = -this.v.y;
    }

    this.x += this.v.x;
    this.y += this.v.y;
    this.x = Math.min(Math.max(this.r, this.x), width - this.r);
    this.y = Math.min(Math.max(this.r, this.y), height - this.r);
  }
  draw() {
    fill(palets[opts.Palette - 1](this.fill).darken(opts.Darken).hex());
    circle(this.x, this.y, this.r * 2);
  }
}

const gui = new dat.GUI();
var opts = {
  Darken: 0.5,
  BlendMode: true,
  Palette: 2,
  Balls: window.innerWidth * 0.2
};
gui.add(opts, 'Darken', 0, 3);
gui.add(opts, 'BlendMode');
gui.add(opts, 'Palette', 1, 3, 1);
gui.add(opts, 'Balls', 10, 500, 1).onChange(() => {
  // Push more balls
  if (opts.Balls > balls.length) {
    for (let i = 0; i < (opts.Balls - balls.length); i++) {
      balls.push(new Ball());
    }
  } else { // Remove balls
    balls.splice(opts.Balls);
  }
});

const balls = [];
function setup () {
  createCanvas(windowWidth, windowHeight);
  noStroke();
  blendMode(SCREEN);
  for (let i = 0; i < opts.Balls; i++) {
    balls.push(new Ball());
  }
}
function windowResized () {
  resizeCanvas(windowWidth, windowHeight);
}

function draw () {
  clear();
  background(0);
  blendMode(opts.BlendMode ? SCREEN : BLEND);
  balls.forEach((b) => {
    b.update();
    b.draw();
  });
}

      

