

App = {
    loading: false,
    contracts: {},
    // nickName : 'Set a NickName',
    
    load: async () => {
    // Load app...
      await App.loadWeb3()
      await App.loadAccount()
      await App.loadContract()
      await App.render()
    },
  
    // https://medium.com/metamask/https-medium-com-metamask-breaking-change-injecting-web3-7722797916a8
    loadWeb3: async () => {
    //   var Web3 = require('web3');
      if (typeof web3 !== 'undefined') {
        App.web3Provider = web3.currentProvider
        web3 = new Web3(web3.currentProvider)
      } else {
        window.alert("Please connect to Metamask.")
      }
      // Modern dapp browsers...
      if (window.ethereum) {
        window.web3 = new Web3(ethereum)
        try {
          // Request account access if needed
          await ethereum.enable()
          // Acccounts now exposed
          web3.eth.sendTransaction({/* ... */})
        } catch (error) {
          // User denied account access...
        }
      }
      // Legacy dapp browsers...
      else if (window.web3) {
          
        App.web3Provider = web3.currentProvider
        window.web3 = new Web3(web3.currentProvider)
        // Acccounts always exposed
        web3.eth.sendTransaction({/* ... */})
      }
      // Non-dapp browsers...
      else {
        console.log('Non-Ethereum browser detected. You should consider trying MetaMask!')
      }
    },
  
    loadAccount: async () => {
      // Set the current blockchain account
    //   App.account = web3.eth.accounts[0]
      const accounts = await web3.eth.getAccounts();
      App.account = accounts[0];
    },
  
    loadContract: async () => {
      // Create a JavaScript version of the smart contract
      const todoList = await $.getJSON('TodoList.json')
      App.contracts.TodoList = TruffleContract(todoList)
      App.contracts.TodoList.setProvider(App.web3Provider)
  
      // Hydrate the smart contract with values from the blockchain
      App.todoList = await App.contracts.TodoList.deployed()
    },
  
    render: async () => {

      // Prevent double render
      if (App.loading) {
        return
      }
  
      // Update app loading state
      App.setLoading(true)
  
      // Render Account
      $('#account').html(App.account)
      
      // const userCount = await App.todoList.userCount()
      // for (var i = 1; i <= userCount; i++) {
      //   // Fetch the user nick from the blockchain
      //   // const user = await App.todoList.users(i);
      //   // const userAddre = user[1];
      //   // const nickName = user[2]
      //   console.log(user)

      //   if (userAddre == App.account){
      //     $('.nickNameAcc').html(nickName)
      //   }
      // }


      // Load Balance of User
      const balanWei = await web3.eth.getBalance (App.account)   
      const balanceEther = await Web3.utils.fromWei(balanWei, 'ether');
      $('#balance').html(balanceEther)

      // // Try to Get Transaction history
      // let block = await web3.eth.getBlock('latest');
      // console.log('block: ', block);
      // let number = block.number;
      // let transactions = block.transactions;
      // console.log('Search Block: ' + transactions);

      // if (block != null && block.transactions != null) {
      //     for (let txHash of block.transactions) {
      //         let tx = await web3.eth.getTransaction(txHash);
      //         console.log("tx: ", tx )
      //         if (App.account == tx.from) {
      //             console.log("from: " + tx.from + " to: " + tx.to + " value: " + tx.value);
      //         }
      //     }
      // }
 
      // Render Tasks
      await App.renderTasks()
  
      // Update loading state
      App.setLoading(false)
    },
  
    renderTasks: async () => {
      
      // Load the total task count from the blockchain
      const taskCount = await App.todoList.taskCount()
      const $taskTemplate = $('.taskTemplate')
  
      // Render out each task with a new task template
      for (var i = 1; i <= taskCount; i++) {
        // Fetch the task data from the blockchain
        const task = await App.todoList.tasks(i)
        
        const taskId = task[0].toNumber()
        const taskContent = task[1]
        const taskCompleted = task[2]
        const nickName = task[4]
        console.log(task[0].toNumber())
        
        console.log(nickName)

        // Create the html for the task
        const $newTaskTemplate = $taskTemplate.clone()
        $newTaskTemplate.find('.content').html(taskContent)
        $newTaskTemplate.find('.owner').html(nickName)

        $newTaskTemplate.find('input')
                        .prop('name', taskId)
                        // .prop('order', taskId)
                        // .prop('checked', taskCompleted)
                        .on('click', App.toggleCompleted)
                        .prop('owner', nickName)
                        
        // console.log($newTaskTemplate.prop(order))
 
  
        // Put the task in the correct list
        if (taskCompleted) {
          $('#completedTaskList').append($newTaskTemplate)
        } else {
          $('#taskList').append($newTaskTemplate)
        }
  
        // Show the task
        $newTaskTemplate.show()
      }
      const del = $('#delete').on('click', App.toggleCompleted)
      // const edit = $('#edit').on('click', App.edit)

    },

    createTask: async () => {
        App.setLoading(true)
        const content = $('#newTask').val()
        const nickName = $('#nickName').val()
        console.log(nickName)

        await App.todoList.createTask(content, nickName, {from: App.account})
        // await App.todoList.createTask(content)
        window.location.reload()
      },

    toggleCompleted: async (e) => {
        App.setLoading(true)
        const taskId = e.target.name
        await App.todoList.toggleCompleted(taskId, {from: App.account})
        // await App.todoList.toggleCompleted(taskId)
        window.location.reload()
    },

    // function nickName () {
      //   document.getElementById('nickName').addEventListener("submit", function(e) {
      //     e.preventDefault(); // before the code
      //     /* do what you want with the form */
      //     const nickName = $('#nickName').val()
      //     const owner = $('#owner')
      //     owner = nickName
      //     console.log(nickName)
      //     // Should be triggered on form submit
      //     console.log('hi');
        
      // });
      // }

    setNickName: async () => {
      App.setLoading(true)
      nickName = $('#nickName').val();
      console.log(nickName)
      // $('.nickNameAcc').html(nickName)
      await App.todoList.setNickName(nickName, {from: App.account})
      
// Render NickName
      // await App.todoList.toggleCompleted(taskId)
      window.location.reload()
  },
  
    setLoading: (boolean) => {
      App.loading = boolean
      const loader = $('#loader')
      const content = $('#content')
      if (boolean) {
        loader.show()
        content.hide()
      } else {
        loader.hide()
        content.show()
      }
    }
  }
  
  $(() => {
    $(window).load(() => {
      App.load()
    })
  })
  
