pragma solidity ^0.5.0;

contract TodoList {
    uint public taskCount = 0;
    uint public userCount = 0;
    // string public nickName = "Anonymous*";
    address public sender = msg.sender;

    struct Task {
        uint id;
        string content;
        bool completed;
        address sender;
        string nickName;
    }

    // struct User {
    //     uint id;
    //     address owner;
    //     string nickName;
    // }

    mapping(uint => Task) public tasks;
    //mapping(address => Task) private Users;
    // mapping(uint => User) public users;

    // event UserCreated(
    //     uint id,
    //     address owner,
    //     string nickName
    // );

    event TaskCreated(
        uint id,
        string content,
        bool completed,
        address sender,
        string nickName
    );
    
    constructor() public {
        createTask("<<**WELCOME to HELLENIC DAPP CHANEL**>>", "StAlarm");
        //setNickName("HDC");
    }

    function createTask(string memory _content,string memory _nickName) public {
        taskCount ++;
        tasks[taskCount] = Task(taskCount, _content, false, sender, _nickName);
        emit TaskCreated(taskCount, _content, false, sender, _nickName);
    }
    
    //function setNickName(string memory _nickName) public {
        // userCount ++;
        // users[userCount] = User(userCount, sender, _nickName);
        // emit UserCreated(userCount, sender, _nickName);
        
        //Task storage task = Users[sender];

        // for (uint i = 0; i<= Users[].length; i++ ) {
        // Users[sender].nickName = _nickName;
        //}
    //}

    function toggleCompleted(uint taskId) public {
            
        Task storage task = tasks[taskId];
        
        if ( task.completed == false ){
            task.completed = true;
        }
        else{
            task.completed = false;
        }
        // task.completed = taskId; 
        // tasks[id].completed = 
        // Task(id, _completed, true);
    }
    

}

